Hello and welcome to my data analysis portfolio project! In this project, I created a database using SQL to manage and analyze a large dataset on Sales of a SuperMarkret. The dataset used in this project was obtained from a real-world scenario, and it required significant cleaning and transformation to be usable in the database.

Throughout the project, I utilized various SQL techniques, such as creating tables, inserting data, and querying the data. I also implemented various advanced SQL concepts like joining tables, using subqueries, and creating views in further repostories.

The final result is a well-structured and optimized database that can be used for various analyses and business intelligence tasks. I hope you find this project informative and insightful. Thank you for taking the time to review my work.
